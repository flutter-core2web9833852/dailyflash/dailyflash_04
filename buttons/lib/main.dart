import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State createState() => _MainApp();
}

class _MainApp extends State {
  int count = 1;

  void nextQuestion() {
    setState(() {
      count++;
    });
  }

  Scaffold question() {
    if (count == 1) {
      return Scaffold(
        body: Center(
          child: ElevatedButton(
              onPressed: () {},
              style: const ButtonStyle(
                shadowColor: MaterialStatePropertyAll(
                  Colors.red,
                ),
              ),
              child: const Text("Button 1")),
        ),
      );
    } else if (count == 2) {
      return Scaffold(
          body: Center(
        child: ElevatedButton(
            onPressed: () {},
            style: ElevatedButton.styleFrom(
                shape: const CircleBorder(side: BorderSide(width: 250)),
                padding: EdgeInsets.all(40)),
            child: const Text("Button 2")),
      ));
    } else if (count == 3) {
      return Scaffold(
        body: SizedBox(
          width: 100,
          height: 50,
          // padding: const EdgeInsets.all(20),
          child: FloatingActionButton(
            onPressed: () {},
            child: const Row(
              children: [Text("Khushal"), Icon(Icons.man)],
            ),
          ),
        ),
      );
    } else if (count == 4) {
      return Scaffold(
        body: Center(
          child: FloatingActionButton(
              onPressed: () {
                setState(() {});
              },
              hoverColor: Color.fromARGB(255, 255, 242, 0),
              child: const Text("Button 4")),
        ),
      );
    } else {
      return Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                    onLongPress: () {},
                    child: FloatingActionButton(
                      onPressed: () {},
                      splashColor: Colors.amber,
                      child: const Text("Long press"),
                    ))
              ],
            )
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: question(),
      floatingActionButton: FloatingActionButton(
        onPressed: nextQuestion,
        child: const Text("Next"),
      ),
    ));
  }
}
